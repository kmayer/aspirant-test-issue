<?php

namespace App\Support;

use UltraLite\Container\Container;

/**
 * Interface ServiceProviderInterface.
 */
interface ServiceProviderInterface
{
    public function register(Container $container): void;
}
