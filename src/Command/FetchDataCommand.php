<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\Movie;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use DOMDocument;
use Exception;
use GuzzleHttp\Psr7\Request;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Log\LoggerInterface;
use SimpleXMLElement;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\RuntimeException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

/**
 * Class FetchDataCommand.
 */
class FetchDataCommand extends Command
{
    private const SOURCE = 'https://trailers.apple.com/trailers/home/rss/newtrailers.rss';
    private const COUNT_OF_TRAILERS_MIN = 1;
    private const COUNT_OF_TRAILERS_MAX = 10;

    /**
     * @var string
     */
    protected static $defaultName = 'fetch:trailers';

    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * @var LoggerInterface
     */
    private $logger;

    /**
     * @var EntityManagerInterface
     */
    private $doctrine;

    /**
     * FetchDataCommand constructor.
     */
    public function __construct(ClientInterface $httpClient, LoggerInterface $logger, EntityManagerInterface $em, string $name = null)
    {
        parent::__construct($name);
        $this->httpClient = $httpClient;
        $this->logger = $logger;
        $this->doctrine = $em;
    }

    public function configure(): void
    {
        $this
            ->setDescription('Fetch data from iTunes Movie Trailers')
            ->addArgument('source', InputArgument::OPTIONAL, 'Overwrite source')
            ->addOption(
                'count',
                'c',
                InputOption::VALUE_OPTIONAL,
                sprintf('Count of trailers (between %d and %d)', self::COUNT_OF_TRAILERS_MIN, self::COUNT_OF_TRAILERS_MAX),
                self::COUNT_OF_TRAILERS_MAX
            );
    }

    /**
     * @throws Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->logger->info(sprintf('Start %s at %s', __CLASS__, (string) date_create()->format(DATE_ATOM)));

        $source = $this->getSource($input);
        $count = $this->getCount($input);

        $io = new SymfonyStyle($input, $output);
        $io->title(sprintf('Fetch data from %s', $source));

        try {
            $response = $this->httpClient->sendRequest(new Request('GET', $source));
        } catch (ClientExceptionInterface $e) {
            throw new RuntimeException($e->getMessage());
        }
        if (($status = $response->getStatusCode()) !== 200) {
            throw new RuntimeException(sprintf('Response status is %d, expected %d', $status, 200));
        }
        $contents = $response->getBody()->getContents();
        $this->processXml($contents, $count);

        $this->logger->info(sprintf('End %s at %s', __CLASS__, date_create()->format(DATE_ATOM)));

        return 0;
    }

    /**
     * @throws Exception
     */
    protected function processXml(string $contents, int $count): void
    {
        $xml = (new SimpleXMLElement($contents))->children();
        if (!property_exists($xml, 'channel')) {
            throw new RuntimeException('Could not find \'channel\' element in feed');
        }

        $items = $xml->channel->item;
        $itemsCount = $items->count();
        for ($i = 0; $i < $count && $i < $itemsCount; ++$i) {
            $item = $items[$i];
            $trailer = $this->getMovie((string) $item->title)
                ->setTitle((string) $item->title)
                ->setDescription((string) $item->description)
                ->setLink((string) $item->link)
                ->setPubDate($this->parseDate((string) $item->pubDate))
                ->setImage($this->parseImage($item));

            $this->doctrine->persist($trailer);
        }

        $this->doctrine->flush();
    }

    /**
     * @throws Exception
     */
    protected function parseDate(string $dateString): DateTime
    {
        return new DateTime($dateString);
    }

    protected function getMovie(string $title): Movie
    {
        $item = $this->doctrine->getRepository(Movie::class)->findOneBy(['title' => $title]);

        if ($item === null) {
            $this->logger->info('Create new Movie', ['title' => $title]);
            $item = new Movie();
        } else {
            $this->logger->info('Move found', ['title' => $title]);
        }

        return $item;
    }

    private function parseImage(SimpleXMLElement $item): ?string
    {
        $content = $item->children('content', true)->encoded;
        $domDocument = new DOMDocument();
        $domDocument->loadHTML((string) $content);
        $firstImage = $domDocument->getElementsByTagName('img')->item(0);
        if (!empty($firstImage)) {
            $domElementImg = simplexml_import_dom($firstImage);

            return (string) $domElementImg['src'];
        }

        return null;
    }

    /**
     * @throws \RuntimeException
     */
    private function getSource(InputInterface $input): string
    {
        $source = self::SOURCE;
        if ($input->getArgument('source')) {
            $source = $input->getArgument('source');
        }

        if (!is_string($source)) {
            throw new RuntimeException('Source must be string');
        }

        return $source;
    }

    /**
     * @throws \RuntimeException
     */
    private function getCount(InputInterface $input): int
    {
        $count = (int) $input->getOption('count');
        $this->logger->debug(sprintf('Count of trailers %d', $count));
        if ($count < self::COUNT_OF_TRAILERS_MIN || $count > self::COUNT_OF_TRAILERS_MAX) {
            throw new \RuntimeException(sprintf('Count of trailers should be between %d and %d', self::COUNT_OF_TRAILERS_MIN, self::COUNT_OF_TRAILERS_MAX));
        }

        return $count;
    }
}
