<?php

declare(strict_types=1);

namespace App\Controller;

use App\Repository\MovieRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpInternalServerErrorException;
use Twig\Environment;

/**
 * Class HomeController.
 */
class HomeController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var MovieRepository
     */
    private $movieRepository;

    /**
     * HomeController constructor.
     */
    public function __construct(Environment $twig, MovieRepository $movieRepository)
    {
        $this->twig = $twig;
        $this->movieRepository = $movieRepository;
    }

    /**
     * @throws HttpInternalServerErrorException
     */
    public function index(ServerRequestInterface $request, ResponseInterface $response): ResponseInterface
    {
        try {
            $data = $this->twig->render('home/index.html.twig', [
                'trailers' => $this->fetchData(),
                'controllerInfo' => sprintf('%s::%s', __CLASS__, __FUNCTION__),
            ]);
        } catch (\Exception $e) {
            throw new HttpInternalServerErrorException($request, $e->getMessage(), $e);
        }

        $response->getBody()->write($data);

        return $response;
    }

    protected function fetchData(): Collection
    {
        $data = $this->movieRepository->lastTen();

        return new ArrayCollection($data);
    }
}
