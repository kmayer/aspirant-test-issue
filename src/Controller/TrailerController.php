<?php

namespace App\Controller;

use App\Repository\MovieRepository;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Slim\Exception\HttpBadRequestException;
use Slim\Exception\HttpNotFoundException;
use Twig\Environment;

class TrailerController
{
    /**
     * @var Environment
     */
    private $twig;

    /**
     * @var MovieRepository
     */
    private $movieRepository;

    /**
     * TrailerController constructor.
     */
    public function __construct(Environment $twig, MovieRepository $movieRepository)
    {
        $this->twig = $twig;
        $this->movieRepository = $movieRepository;
    }

    /**
     * @param array $args
     *
     * @throws HttpBadRequestException
     */
    public function trailerInfo(ServerRequestInterface $request, ResponseInterface $response, $args = []): ResponseInterface
    {
        $trailerId = $args['id'];
        $movie = $this->movieRepository->find($trailerId);
        if (empty($movie)) {
            throw new HttpNotFoundException($request, "trailer with id '$trailerId' does not exists");
        }
        $data = $this->twig->render('trailers/trailerInfo.html.twig', [
            'trailer' => $movie,
        ]);
        $response->getBody()->write($data);

        return $response;
    }
}
