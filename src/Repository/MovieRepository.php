<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Movie;
use Doctrine\ORM\EntityRepository;

/**
 * Class MovieRepository.
 */
class MovieRepository extends EntityRepository
{
    /**
     * @return Movie[]
     */
    public function lastTen(): array
    {
        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select(['m'])
            ->from(Movie::class, 'm')
            ->orderBy('m.pubDate', 'desc')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult();
    }
}
